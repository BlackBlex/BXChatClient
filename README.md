**BXChat Client**
===================


Es un chat escrito en java que contiene un sistema de plugins que complementerá el sistema.


*Requiere:* [BlexLib](https://gitlab.com/BlackBlex/blexlib)

*Importante:* La rama (main) contiene codigo solo para ejecutar en Java 9 en adelante, si quiere usar Java 8, use la rama Java 8

--------
**Caracteristicas:**

 - Enviar mensajes a todos los usuarios conectados.
 - Sistema de plugins.
 - Distinción de mensajes propios.

--------
**Plugins disponibles:**

 - **SendPMMessage** ~ Implementa un chat privado.
 - **SendFile** ~ Agrega la posibilidad de mandar archivos a un usuario en especifico.
 - **EmojiChooser** ~ Agrega un selector de emojis.
 - **FontChooser** ~ Agrega la posibilidad de cambiar el tamaño, el estilo de la fuente.

--------

**Nota:** *Se irá añadiendo más características conforme se vaya avanzando en su desarrollo. **No cuenta con sistema de seguridad, se implementará más adelante***

--------

Para ejecutar se debe:
1. Clonar el repositorio
2. Abrir con VSCode y tener instalado [Java pack Extension](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack)
3. Abrir el archivo Main.java ubicado en **src/app**
4. Ejecutar el archivo desde VSCode

--------

Imagen:

Conexiones

![](images/conexiones.png)

Servidor lleno

![](images/servidor_lleno.png)

Charla

![](images/charla.png)

Desconexiones

![](images/desconexion.png)

Registro en la terminal

![](images/terminal_log.png)


--------
 BXChat | Chat básico con sistema de plugins

 Cliente escrito en java

 Author: @BlackBlex (BlackBlex)

 License: General Public License (GPLv3) | http://www.gnu.org/licenses/
